# ddd-ui

"ddd-ui" for Duniter Datapods Demo UI

```sh
pnpm install
pnpm dev
```

## Publish

```sh
# build app to "dist" folder
pnpm build
# publish to ipfs and add history
# dist: build folder to publish
# 0.0: version number
# init: version comment, init is a special case for first commit
pnpm exec tsx src/scripts/publish.ts "dist" "0.0" "init"
```

TODO

- [x] fix resolving ipns on client
- [x] docker: add key to publish history in kubo keystore
- [x] check Peering.Peers Kubo option
- [x] add # to routing
- [ ] crypto : replace polkadot by libp2p keyring to reduce bundle size
- [ ] add --to-files option to "pin" latest ui in MFS
- [ ] wildcard certificate to serve sites
- [ ] add GraphQL subscription to see the recent profiles
- [ ] load C+ profile from currently selected account
- [ ] add member pseudo to db

## GraphQL Schema

```sh
# get schema
pnpm exec get-graphql-schema https://datapod.coinduf.eu/v1/graphql > src/schema.graphql
# pnpm exec graphql-codegen download-schema https://datapod.coinduf.eu/v1/graphql --output src/schema.json
# pnpm exec graphql-codegen --config codegen.ts
```

TODO typegen
