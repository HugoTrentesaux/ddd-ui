import { CID } from 'multiformats'

// this file contains dependencies to group in a lib
// for the moment they are updated manually from datapod types

export interface IndexRequest {
  pubkey: string
  time: number
  kind: string
  data: CID | null
}

export interface SignedIndexRequest extends IndexRequest {
  sig: string
}

// build payload to sign
export function buildStringPayload(ir: IndexRequest) {
  let payload = `prefix: Duniter Datapod
time: ${ir.time}
kind: ${ir.kind}
`
  if (ir.data)
    payload += `data: ${ir.data.toV1()}
`
  return payload
}

// new Cplus Profile inspired from
// https://doc.e-is.pro/cesium-plus-pod/REST_API.html
// fields marked as "moved" are moved to index request
// only relevant data remains
export interface CplusProfile {
  // --- mandatory
  // version → moved (TODO)
  title: string
  description: string
  // time → moved (timestamp)
  // issuer → moved (pubkey)
  // hash → moved (cid)
  // signature → moved
  // --- optional
  socials?: Social[]
  tags?: string[]
  avatar?: CID
}

// social
interface Social {
  type: string
  url: string
}

// ===

// describe interface history
export interface Version {
  // previous version history
  previous: Version | null
  // cid of current app
  current: CID
  // version number
  version: string
  // comment
  comment: string
}

// ===

// database types
// TODO generate from schema

export interface DbCplusProfile {
  index_request_cid: string
  data_cid: string
  time: string
  pubkey: string
  title: string
  description: string
  socials?: Social[]
  tags?: string[]
  avatar?: string
  geoloc: string
  data: string
  city: string
}

// ================== tx comment

export interface TxComment {
  /// extrinsic hash as string
  tx_id: string
  /// comment
  comment: string
}
