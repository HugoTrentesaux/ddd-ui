import { CID } from 'multiformats'
import * as multihash from 'multiformats/hashes/digest'
import { blake2b256 } from '@multiformats/blake2/blake2b'

const comment = 'commentaire de transaction' + String.fromCharCode(0x0a)
const commentBytes = Buffer.from(comment, 'ascii')
const commentHash = await blake2b256.encode(commentBytes)
const commentHashHex = Buffer.from(commentHash).toString('hex')

console.log(commentHashHex)

const hexHash = commentHashHex
const hashBytes = Buffer.from(hexHash, 'hex')
const mh = multihash.create(blake2b256.code, hashBytes)
const cid = CID.createV1(0x55, mh)

console.log(cid.toString())
