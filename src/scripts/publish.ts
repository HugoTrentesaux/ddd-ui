import { execSync } from 'child_process'
import { writeFileSync } from 'fs'
import { create } from 'kubo-rpc-client'
import type { KuboRPCClient } from 'kubo-rpc-client'
import { CID } from 'multiformats'
import { resolve } from 'path'
import { IPNS_DDD_UI_HISTORY } from '../consts'
import type { Version } from '../lib'

// this scripts publishes to IPFS and updates IPNS with version history
// this will allow the app user to easily check for updates

// kubo connection
// can use a ssh brigde to connect to a remote node
const KUBO_RPC = 'http://127.0.0.1:5001'
const kubo: KuboRPCClient = create({ url: new URL(KUBO_RPC) })

// publish to history as it is
async function publishHistory(version: Version) {
  const cid = await kubo.dag.put(version)
  console.log('version cid: ' + cid)
  const res = await kubo.name.publish(cid, { key: IPNS_DDD_UI_HISTORY, ttl: '1h', lifetime: '72h' })
  console.log('published to ' + res.name)
}

// push to history based on IPNS
async function pushHistory(version: Version) {
  for await (const name of kubo.name.resolve(IPNS_DDD_UI_HISTORY, { nocache: true })) {
    const cid = CID.parse(name.slice(6))
    const dag = await kubo.dag.get(cid)
    version.previous = dag.value
    await publishHistory(version)
  }
}

// main
const argv = process.argv
if (argv.length == 5) {
  const distpath = resolve(process.argv[2])
  const ver = process.argv[3]
  const comment = process.argv[4]
  // write version number to asset file
  const versionfile = resolve(distpath, 'version.txt')
  writeFileSync(versionfile, ver)
  // upload dist folder to ipfs
  // easier to do with CLI interface than js
  // TODO make it in typescript
  console.log('uploading dist folder...')
  execSync('ipfs --api=/ip4/127.0.0.1/tcp/5001 files rm -r /ddd/ui') // remove files first
  const output = execSync('ipfs --api=/ip4/127.0.0.1/tcp/5001 add -Qr dist --to-files /ddd/ui').toString().trimEnd()
  console.log(`done: [${output}]`)
  const cid = CID.parse(output)
  // WTF:
  // the command does not return, so I need to do this manually
  // const cid = CID.parse('QmYcADujz2ndbKHKjJRbzoVKo4QXoePcTDF5pMxUrXJS8s')

  // build version
  const version: Version = {
    previous: null,
    current: cid,
    version: ver,
    comment: comment,
  }
  // if comment is "init" initialize
  if (comment == 'init') {
    await publishHistory(version)
  } else {
    await pushHistory(version)
  }
} else {
  console.log('pease give args: [dist folder path] [version number] [comment]')
  console.log('run `pnpm build` before')
}
