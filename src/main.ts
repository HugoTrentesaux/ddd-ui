// vue app
import './assets/main.css'
import App from './App.vue'
import router from './router'
import { createApp, ref, type Ref } from 'vue'

// local deps
import { ALL_PEERS, DELEGATED_ROUTING } from './ipfs'
import { gethelia, getlibp2p } from './ipfs'
import type { CustomServiceMap } from './ipfs'
// helia
import { unixfs, type UnixFS } from '@helia/unixfs'
import { dagCbor, type DAGCBOR } from '@helia/dag-cbor'
import { ipns, type IPNS } from '@helia/ipns'
import type { HeliaLibp2p } from 'helia'
// import { pubsub } from '@helia/ipns/routing'
// web3
import { web3Enable } from '@polkadot/extension-dapp'
import type { InjectedAccountWithMeta } from '@polkadot/extension-inject/types'
// libp2p
import type { Libp2p, PubSub } from '@libp2p/interface'
// graphql
import { ApolloClient, createHttpLink, InMemoryCache, type NormalizedCacheObject } from '@apollo/client/core'
import { ApolloClients } from '@vue/apollo-composable'
// other
// import debug from 'debug'

// === DEPENDENCIES ===

// provide dependencies to the app
async function provideDeps(app: any): Promise<null> {
  // enable debug
  // debug.enable('libp2p*')
  // debug.enable('helia:bitswap*')
  // debug.disable()

  // create libp2p conf
  const _libp2p: Libp2p<CustomServiceMap> = await getlibp2p()
  /// expose pubsub feature
  const _pubsub: PubSub = _libp2p.services.pubsub as PubSub
  // create a Helia node
  const _helia: HeliaLibp2p<Libp2p<CustomServiceMap>> = await gethelia(_libp2p)
  // dial
  _libp2p.dial(ALL_PEERS.map((p) => p.addrs).flat())

  /// expose fs interface for images for instance
  // currently it is only in memory
  // but later it would be nice to use datastore-idb to keep things in browser indexed db
  // this would avoid re-fetching content when closing / re-opening
  const heliafs: UnixFS = unixfs(_helia)

  /// expose dag cbor interface for compact object representation
  const heliadag: DAGCBOR = dagCbor(_helia)

  /// expose name resolver
  const helianame: IPNS = ipns(_helia, {
    routers: DELEGATED_ROUTING,
    // helia(_helia.routing), // already insterted by helia
    // pubsub(_helia), // trying this but does not work
    // that's why I have to use delegated routing
  })

  // ask brower extension for web3 support
  web3Enable('ddd')

  // account selected by user
  const account: Ref<InjectedAccountWithMeta | null> = ref(null)

  // HTTP connection to the API
  const httpLink = createHttpLink({
    uri: 'https://datapod.coinduf.eu/v1/graphql',
  })
  // Cache implementation
  const cache = new InMemoryCache()
  // Create the apollo client
  const apolloClient: ApolloClient<NormalizedCacheObject> = new ApolloClient({
    link: httpLink,
    cache,
  })

  // provide all dependencies
  app.provide('libp2p', _libp2p)
  app.provide('helia', _helia)
  app.provide('heliafs', heliafs)
  app.provide('heliadag', heliadag)
  app.provide('helianame', helianame)
  app.provide('pubsub', _pubsub)
  app.provide('account', account)
  app.provide('apolloClient', apolloClient)
  app.provide(ApolloClients, { default: apolloClient })

  return null
}

// === APP ===

const app = createApp(App)
app.use(router)
provideDeps(app).then(() => app.mount('#app'))
