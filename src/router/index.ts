import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue'),
    },
    {
      path: '/upload',
      name: 'upload',
      component: () => import('../views/UploadView.vue'),
    },
    {
      path: '/download',
      name: 'download',
      component: () => import('../views/DownloadView.vue'),
    },
    {
      path: '/network',
      name: 'network',
      component: () => import('../views/NetworkView.vue'),
    },
    {
      path: '/pubsub',
      name: 'pubsub',
      component: () => import('../views/PubsubView.vue'),
    },
    {
      path: '/graphql',
      name: 'graphql',
      component: () => import('../views/GraphqlView.vue'),
    },
    {
      path: '/gatewayupload',
      name: 'gatewayupload',
      component: () => import('../views/GatewayUpload.vue'),
    },
  ],
})

export default router
