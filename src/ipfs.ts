import { createHelia } from 'helia'
import { bootstrap } from '@libp2p/bootstrap'
import { webTransport } from '@libp2p/webtransport'
import { webSockets } from '@libp2p/websockets'
import { createLibp2p } from 'libp2p'
import { gossipsub, type GossipSubComponents, type GossipsubEvents } from '@chainsafe/libp2p-gossipsub'
import { bitswap, trustlessGateway } from '@helia/block-brokers'
import { multiaddr, type Multiaddr } from '@multiformats/multiaddr'
import { type HeliaLibp2p } from 'helia'
import { type Libp2p, type PeerId, type PubSub } from '@libp2p/interface'
import { createHeliaHTTP } from '@helia/http'
import { delegatedHTTPRouting, libp2pRouting } from '@helia/routers'
import { noise } from '@chainsafe/libp2p-noise'
import { yamux } from '@chainsafe/libp2p-yamux'
import { identify, type Identify } from '@libp2p/identify'
import { kadDHT, type KadDHT } from '@libp2p/kad-dht'
import { peerIdFromString } from '@libp2p/peer-id'
import type { AddrInfo } from '@chainsafe/libp2p-gossipsub/types'

// import { keychain, type Keychain } from '@libp2p/keychain'
// import { webRTC } from '@libp2p/webrtc'

// local peer
const LOCAL_PEER: AddrInfo = {
  id: peerIdFromString('12D3KooWF44SaSomGuUSNycgpkRwQcxcsMYeNbtn6XCHPR2ojodv'),
  addrs: [
    '/ip4/127.0.0.1/tcp/4001/p2p/12D3KooWF44SaSomGuUSNycgpkRwQcxcsMYeNbtn6XCHPR2ojodv',
    '/ip4/127.0.0.1/tcp/4002/ws/p2p/12D3KooWF44SaSomGuUSNycgpkRwQcxcsMYeNbtn6XCHPR2ojodv',
    '/ip4/127.0.0.1/udp/4001/quic-v1/p2p/12D3KooWF44SaSomGuUSNycgpkRwQcxcsMYeNbtn6XCHPR2ojodv',
    '/ip4/127.0.0.1/udp/4001/quic-v1/webtransport/certhash/uEiC6q4csXlqjvPg_z28SK2fu59NKwOLBDud7j-6Cdn958Q/certhash/uEiBBTMnjRsYnA2j80DY5hS831YQYPMHDtWwIUv8-d5EB1A/p2p/12D3KooWF44SaSomGuUSNycgpkRwQcxcsMYeNbtn6XCHPR2ojodv',
  ].map(multiaddr),
}

// Hugo
const PEER_HUGO: AddrInfo = {
  id: peerIdFromString('12D3KooWFp4JsoDo5FX8CFLtyJjaWWRZ8q3gr8uT2s9To2GYzRNA'),
  addrs: [
    '/dns/datapod.coinduf.eu/tcp/4001/p2p/12D3KooWFp4JsoDo5FX8CFLtyJjaWWRZ8q3gr8uT2s9To2GYzRNA',
    '/dns/websocket.datapod.coinduf.eu/tcp/443/wss/p2p/12D3KooWFp4JsoDo5FX8CFLtyJjaWWRZ8q3gr8uT2s9To2GYzRNA',
    '/dns/datapod.coinduf.eu/udp/4001/quic-v1/p2p/12D3KooWFp4JsoDo5FX8CFLtyJjaWWRZ8q3gr8uT2s9To2GYzRNA',
    '/dns/datapod.coinduf.eu/udp/4001/quic-v1/webtransport/certhash/uEiAPENF9zFad6dUjq1Wi3JtnSZ7C3Dsb7OVW3_0HjamIuA/certhash/uEiAzRcDv8XF-ECBzHhBOWnZgWX9D2--ZyODFJsFpnKS-jA/p2p/12D3KooWFp4JsoDo5FX8CFLtyJjaWWRZ8q3gr8uT2s9To2GYzRNA',
  ].map(multiaddr),
}

// Poka TODO

// all peers (useful to switch local node when developing)
// export const ALL_PEERS = [LOCAL_PEER]
export const ALL_PEERS = [PEER_HUGO]

// local gateway for files retreival
export const LOCAL_GATEWAY = ['http://localhost:8080']
export const PREFERED_GATEWAYS = ['https://gateway.datapod.coinduf.eu']
export const GATEWAYS = PREFERED_GATEWAYS // .concat(LOCAL_GATEWAY)

// delegated routing
const LOCAL_ROUTERS = LOCAL_GATEWAY
const DELEGATED_ROUTERS = PREFERED_GATEWAYS // .concat(LOCAL_ROUTERS) //.concat(['https://delegated-ipfs.dev'])
export const DELEGATED_ROUTING = DELEGATED_ROUTERS.map(delegatedHTTPRouting)

// pubsub topic used for message exchange
export const TOPIC = 'ddd'

// type alias
export type CustomServiceMap = {
  pubsub: PubSub<GossipsubEvents>
  identify: Identify
  aminoDHT: KadDHT
  // keychain: Keychain
}

// create libp2p conf
export async function getlibp2p(): Promise<Libp2p<CustomServiceMap>> {
  return createLibp2p({
    peerDiscovery: [
      bootstrap({
        list: ALL_PEERS.map((p) => p.addrs.map(toString)).flat(),
      }),
    ],
    transports: [
      webTransport(), // only works in chrome, not in firefox
      // webRTC() // works in firefox, but not in go lib yet, will come soon
      webSockets({ filter: (a) => a }), // allow non-tls connections
    ],
    services: {
      pubsub: gossipsub({
        directPeers: ALL_PEERS,
      }),
      identify: identify(),
      aminoDHT: kadDHT({
        protocol: '/ipfs/kad/1.0.0',
        // clientMode: true, // no! this prevents peers from fetching blocks
      }),
      // keychain: keychain() // TODO use Ğ1 key in the keychain as known peer
    },
    connectionGater: {
      // prevent connection outside local whitelist
      // denyDialMultiaddr: async (m: Multiaddr) => !PEER_TRUSTLIST.includes(m.toString())
      // allow connecting to local peer
      denyDialMultiaddr: async () => false,
      denyOutboundConnection: async () => false,
      // filter peers to add in peer storage
      // filterMultiaddrForPeer: (p: PeerId, m: Multiaddr) => ALL_PEERS.includes(m.toString())
    },
    connectionManager: {
      // make sure we stay online
      // minConnections: 5
      // autoDialInterval: 1
    },
    connectionEncryption: [noise()],
    streamMuxers: [yamux()],
    // contentRouters: [delegatedHTTPRouting()]
  })
}

// create a full libp2p Helia node
export function gethelia<T extends Libp2p>(libp2p: T): Promise<HeliaLibp2p<T>> {
  return createHelia({
    libp2p: libp2p,
    blockBrokers: [
      // trustlessGateway({ gateways: GATEWAYS }) // datapod gateways
      bitswap(), // does not work at the moment in browser
    ],
  })
}

// create a http Helia node using delegated routing
export function getheliahttp() {
  return createHeliaHTTP({
    routers: DELEGATED_ROUTING,
    blockBrokers: [
      trustlessGateway({ gateways: GATEWAYS }), // datapod gateways
      bitswap(),
    ],
  })
}
