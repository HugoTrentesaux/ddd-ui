// document describing cesium plus profile
export const UPDATE_CESIUM_PLUS_PROFILE = 'bafkreigi5phtqpo6a2f3tx4obaja4fzevy3nyvnl4bnkcxylyqnfeowzbm'
export const DELETE_CESIUM_PLUS_PROFILE = 'bafkreic5bv5ytl7zv5rh5j2bd5mw6nfrn33mxhiobgmpsiu65yjw3eeduu'
export const TRANSACTION_COMMENT = 'bafkreiegjt5mrfj2hshuw6koejdfiykq57mzjeprfckxj5zpxxtqj4qzeu'

// 1. export from local node
// ipfs key export ddd-ui-history --format=pem-pkcs8-cleartext
// 2. import to remote node using ssh bridge
// ipfs --api=/ip4/127.0.0.1/tcp/5002 key import ddd-ui-history ddd-ui-history.pem --format=pem-pkcs8-cleartext
// ipfs name publish --key=ddd-ui-history /ipfs/QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nn
export const IPNS_DDD_UI_HISTORY = 'k51qzi5uqu5dkgpcm5a6lu2sijgm2ahu2f0c4mets99m52bf9rm6nmbutxbbi5'
